from enum import Enum

from sqlalchemy import select

from core.db.session import async_session_factory
from core.utils.token import Token
from app.v1.models.role_model import RoleModel

class Role(Enum):
    Admin=1
    Agency=2
    Journalist=3
    User=4



class GetRole():
    @staticmethod
    async def getUserDetails(token:str):
        print(token)
        details=Token.get_details(token)
        print(details)
        async with async_session_factory() as session:
            role = await session.execute(select(RoleModel).where(RoleModel.role_id == details["role_id"]))
            role= role.scalars().first()
            role=role.role_name
        return {"user_id":details["user_id"],"role":role}









