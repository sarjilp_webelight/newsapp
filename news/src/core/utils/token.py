from datetime import datetime, timedelta

from fastapi import HTTPException
from jose import jws,jwt
from config import config

SECRET_KEY=config.SECRET_KEY
ACCESS_TOKEN_EXPIRE_MINUTES=config.ACCESS_TOKEN_EXPIRE_MINUTES
ALGORITHM = config.ALGORITHM



class Token:
    @staticmethod
    def get_token(user_dict:dict):
        access_time = timedelta(ACCESS_TOKEN_EXPIRE_MINUTES)
        expire = datetime.utcnow() + timedelta(ACCESS_TOKEN_EXPIRE_MINUTES)
        user_dict.update({"exp":expire})
        print(user_dict)
        print(type(user_dict))
        encoded_jwt=jwt.encode(user_dict,SECRET_KEY,algorithm=ALGORITHM)
        return encoded_jwt

    @staticmethod
    def get_dict(token:str):
        token=token[7:]
        try:
            decoded_token=jwt.decode(token,SECRET_KEY)
        except Exception as e:
            print(e)
            raise HTTPException(status_code=401,detail="JWT is not valid")
        # else:
        #     decoded_token = jwt.decode(token, SECRET_KEY)
        email=decoded_token["email"]
        return email

    @staticmethod
    def get_details(token: str):

        print("token",token)
        try:
            decoded_token = jwt.decode(token, SECRET_KEY)
        except Exception as e:
            print(e)
            raise HTTPException(status_code=401, detail="JWT is not valid")
        # else:
        #     decoded_token = jwt.decode(token, SECRET_KEY)

        return decoded_token