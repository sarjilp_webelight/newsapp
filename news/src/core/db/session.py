from typing import Union

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker

from config import config

engine = create_async_engine(
    config.SQLALCHEMY_DATABASE_URI, pool_recycle=86400, pool_size=40, max_overflow=0
)

async_session_factory = sessionmaker(
    class_=AsyncSession, expire_on_commit=False, bind=engine
)


async def db_session():
    """
    Database Session Generator.

    :return: A database session.
    """
    session: Union[Session, AsyncSession] = async_session_factory()
    yield session
    await session.close()


Base = declarative_base()