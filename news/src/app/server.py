from fastapi import Depends, FastAPI, Request, status
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse


from app import router
from config import AppEnvironment, config
# from core import Cache, CustomException, CustomKeyMaker, Logging, RedisBackend


def init_cors(app: FastAPI) -> None:
    """
    Initialize the CORS middleware.
    """
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


def init_routers(app: FastAPI) -> None:
    """
    Initialize all routers.
    """
    app.include_router(router)



# def init_listeners(app: FastAPI) -> None:
#     """
#     Initialize all routers.
#     """
#
#     @app.exception_handler(CustomException)
#     async def custom_exception_handler(request: Request, exc: CustomException):
#         """
#         Handler for all the custom exceptions raised within the app.
#         """
#         return JSONResponse(
#             status_code=exc.code, content={"status": "FAILED", "message": exc.message}
#         )
#
#     @app.exception_handler(RequestValidationError)
#     async def validation_error_handler(request: Request, exc: RequestValidationError):
#         """
#         Handler for pydantic :class:`ValidationError` exceptions.
#         """
#
#         error_message = []
#         for i in exc.errors():
#             message = "{}: {} ".format(i["loc"][1], i["msg"])
#             error_message.append(message)
#         error_message = ", ".join(error_message)
#         return JSONResponse(
#             status_code=422, content={"status": "FAILED", "message": error_message}
#         )
#
#     @app.exception_handler(Exception)
#     async def exception_handler(_request: Request, exc: Exception):
#         """
#         error handler.
#         """
#         return JSONResponse(
#             status_code=400, content={"status": "FAILED", "message": str(exc)}
#         )
#
#
# def init_cache() -> None:
#     """
#     Cache DB initialization
#     """
#     Cache.init(backend=RedisBackend(), key_maker=CustomKeyMaker())
#

def default_router(app: FastAPI):
    """
    Define the root path of services
    """

    @app.get("/")
    def default_route():
        """
        Define the root path of services
        """
        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content={"message": "Welcome to News App"},
        )


def create_app() -> FastAPI:
    """
    Master function for creating the :class:`FastAPI` APP.

    :return: A FastAPI instance.
    """
    app = FastAPI(
        title="NewsApp",
        description="This is a News App",
        version="1.0.0",
        docs_url=None if config.ENV == AppEnvironment.Production else "/docs",
        redoc_url=None if config.ENV == AppEnvironment.Production else "/redoc",
        #dependencies=[Depends(Logging)],
        # debug=True,
    )
    init_routers(app=app)
    init_cors(app=app)
    #init_listeners(app=app)
    default_router(app=app)
    #init_cache()

    return app


app = create_app()

