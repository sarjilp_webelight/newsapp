from core import Base
from sqlalchemy import Table, ForeignKey, Column

category_news = Table("category_news",Base.metadata,
                      Column("news_id",ForeignKey("News.news_id")),
                      Column("category_id",ForeignKey("Categories.category_id"))
                     )