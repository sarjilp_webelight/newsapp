import uuid

from sqlalchemy import Column, String, Integer, Boolean, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from core.utils.hashing import Hasher
# from app.v1.models import NewsModel
from core.db import Base


class UserModel(Base):
    __tablename__ = "Users"

    user_id = Column(UUID(as_uuid=True), primary_key=True)
    email = Column(String(70),unique=True)
    password =Column(String(70))
    fname = Column(String(70))
    lname = Column(String(70))
    #role_id=Column(Integer)
    role_id = Column(UUID(as_uuid=True),ForeignKey("Roles.role_id"))
    is_active = Column(Boolean,default=False)
    city_id = Column(UUID(as_uuid=True),ForeignKey("City.city_id"))

    news = relationship("NewsModel",lazy="selectin")
    likes = relationship("LikeModel",lazy="selectin")
    comments = relationship("CommentModel", lazy="selectin")

    @classmethod
    def create(cls,email:str,password:str,fname:str,lname:str,city_id:UUID):
        user_id=uuid.uuid4()
        is_active=False
        password=Hasher.get_password_hash(password)
        return cls(user_id=user_id,email=email,password=password,fname=fname,lname=lname,role_id="5b940911-5178-430f-ae63-1f7ca58f6ace",is_active=is_active,city_id=city_id,news=[],likes=[],comments=[])

