from core import Base
from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
import uuid

class CategoryModel(Base):
    __tablename__ = "Categories"

    category_id = Column(UUID(as_uuid=True),primary_key=True)
    category_name = Column(String(70))
    news = relationship("NewsModel", secondary="category_news" ,lazy="selectin", back_populates="category")


    @classmethod
    def create(cls,category_name:str):
        category_id=uuid.uuid4()
        category_name=category_name
        return cls(category_id=category_id,category_name=category_name)
