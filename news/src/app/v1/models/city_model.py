import uuid

from sqlalchemy import Column,String
from sqlalchemy.dialects.postgresql import UUID
from core import Base
from sqlalchemy.orm import relationship


class CityModel(Base):
    __tablename__="City"

    city_id = Column(UUID(as_uuid=True),primary_key=True)
    city_name = Column(String(70))
    users=relationship("UserModel",lazy="selectin")
    news=relationship("NewsModel",lazy="selectin")


    @classmethod
    def create(cls,city_name):
        return cls(city_id=uuid.uuid4(), city_name=city_name,users=[],news=[])