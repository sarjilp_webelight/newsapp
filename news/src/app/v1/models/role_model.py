import uuid

from core import Base
from sqlalchemy import Column,String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

class RoleModel(Base):
    __tablename__="Roles"

    role_id =Column(UUID(as_uuid=True),primary_key=True)
    role_name=Column(String(70))
    users=relationship("UserModel",lazy="selectin")


    @classmethod
    def create(cls,role_name:str):
        return cls(role_id=uuid.uuid4(),role_name=role_name,users=[])