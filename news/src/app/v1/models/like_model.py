import uuid

from sqlalchemy import Column,Integer, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from core import Base

class LikeModel(Base):
    __tablename__="Likes"

    like_id = Column(UUID(as_uuid=True),primary_key=True)
    user_id = Column(UUID(as_uuid=True),ForeignKey("Users.user_id"))
    news_id = Column(UUID(as_uuid=True),ForeignKey("News.news_id"))


    @classmethod
    def create(cls,user_id:UUID,news_id:UUID):
        return cls(like_id=uuid.uuid4(), user_id=user_id, news_id=news_id)