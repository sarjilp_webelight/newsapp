from app.v1.models.user_model import UserModel
from app.v1.models.news_model import NewsModel
from app.v1.models.city_model import CityModel
from app.v1.models.role_model import RoleModel
from app.v1.models.tag_model import TagModel
from app.v1.models.like_model import LikeModel
from app.v1.models.category_model import CategoryModel
from app.v1.models.comment_model import CommentModel
from app.v1.models.category_news_model import category_news
from app.v1.models.otp_model import OtpModel
from app.v1.models.tags_news_model import tags_news

__all__ = ["tags_news","OtpModel","category_news","UserModel","NewsModel","CityModel","RoleModel","TagModel","LikeModel","CategoryModel","CommentModel"]
