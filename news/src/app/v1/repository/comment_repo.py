from typing import List
from uuid import UUID

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.v1.models import CommentModel, NewsModel
from app.v1.schema import CreateCommentRequestSchema
from core import db_session


class CommentRepo:
    def __init__(self, db: AsyncSession = Depends(db_session)):
        self.session = db


    async def getCommentCount(self,news_id:UUID):
        news=await self.session.execute(select(NewsModel).where(NewsModel.news_id == news_id))
        return news.scalars().first()

    async def setCommentCount(self,news:NewsModel):
        self.session.add(news)
        await self.session.commit()
        return {"message":"Commented"}


    async def save(self, comment: CommentModel) -> CommentModel:
        self.session.add(comment)
        await self.session.commit()
        return comment

    async def get_comment(self) -> List[CommentModel]:
        result = await self.session.execute(select(CommentModel))
        print(type(result))
        return result.scalars().all()

    async def get_comment_byid(self, comment_id: UUID) -> CommentModel:
        result = await self.session.execute(select(CommentModel).where(CommentModel.comment_id == comment_id))
        return result.scalars().first()

    async def delete_comment(self, comment_id: UUID):
        comment = await self.get_comment_byid(comment_id=comment_id)
        await self.session.delete(comment)
        await self.session.commit()
        return {"message": "Comment Deleted"}

    async def update_comment(self, comment_id: UUID, request: CreateCommentRequestSchema):
        comment = await self.get_comment_byid(comment_id=comment_id)
        for var, value in vars(request).items():
            setattr(comment, var, value) if value else None

        self.session.add(comment)
        await self.session.commit()
        await self.session.refresh(comment)
        return comment



