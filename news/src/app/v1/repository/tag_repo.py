from typing import List
from uuid import UUID

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.v1.models import TagModel
from app.v1.schema import CreateTagRequestSchema
from core import db_session

class TagRepo:
    def __init__(self,db:AsyncSession=Depends(db_session)):
        self.session=db

    async def save(self,tag:TagModel)->TagModel:
        self.session.add(tag)
        return tag


    async def get_tags(self)->List[TagModel]:
        result = await self.session.execute(select(TagModel))
        return result.scalars().all()

    async def get_tagid(self,tag_id:UUID)->TagModel:
        result=await self.session.execute(select(TagModel).where(TagModel.tag_id==tag_id))
        return result.scalars().first()


    async def delete_tag(self,tag_id:UUID):
        tag=await self.get_tagid(tag_id=tag_id)
        await self.session.delete(tag)
        await self.session.commit()
        return {"message":"Tag Deleted"}



