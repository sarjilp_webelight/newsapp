from typing import List
from uuid import UUID

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.v1.models import CategoryModel
from app.v1.schema import CreateCategoryRequestSchema
from core import db_session

class CategoryRepo:
    def __init__(self,db:AsyncSession=Depends(db_session)):
        self.session=db

    async def save(self,category:CategoryModel)->CategoryModel:
        self.session.add(category)
        await self.session.commit()
        return category


    async def get_category(self)->List[CategoryModel]:
        result = await self.session.execute(select(CategoryModel))
        return result.scalars().all()

    async def get_categoryid(self,category_id:UUID)->CategoryModel:
        result=await self.session.execute(select(CategoryModel).where(CategoryModel.category_id==category_id))
        return result.scalars().first()


    async def delete_category(self,category_id:UUID):
        category=await self.get_categoryid(category_id=category_id)
        await self.session.delete(category)
        await self.session.commit()
        return {"message":"Category Deleted"}


    async def update_category(self,category_id:UUID,request:CreateCategoryRequestSchema):
        category=await self.get_categoryid(category_id=category_id)
        print(category)
        for var,value in vars(request).items():
            setattr(category,var,value) if value else None

        self.session.add(category)
        await self.session.commit()
        await self.session.refresh(category)
        return category
