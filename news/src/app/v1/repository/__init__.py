from app.v1.repository.user_repo import UserRepo
from app.v1.repository.news_repo import NewsRepo
from app.v1.repository.city_repo import CityRepo
from app.v1.repository.role_repo import RoleRepo
from app.v1.repository.tag_repo import TagRepo
from app.v1.repository.like_repo import LikeRepo
from app.v1.repository.category_repo import CategoryRepo
from app.v1.repository.comment_repo import CommentRepo
from app.v1.repository.auth_repo import AuthRepo
from app.v1.repository.category_news_repo import CategoryNewsRepo
from app.v1.repository.tags_news_repo import TagsNewsRepo


__all__=["CategoryNewsRepo","AuthRepo","UserRepo","NewsRepo","CityRepo","RoleRepo","TagRepo","LikeRepo","CategoryRepo","CommentRepo","TagsNewsRepo"]