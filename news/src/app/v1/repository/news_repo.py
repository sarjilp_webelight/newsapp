from typing import List
from uuid import UUID

from fastapi import Depends, HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.v1.models import NewsModel, UserModel, CategoryModel, TagModel
from app.v1.schema import CreateNewsRequestSchema
from core import db_session


class NewsRepo:
    def __init__(self, db: AsyncSession = Depends(db_session)):
        self.session = db

    async def save(self, news: NewsModel) -> NewsModel:
        self.session.add(news)
        await self.session.commit()
        return news

    async def save_category(self, category: CategoryModel):
        return category

    async def check_active_user(self,user_id):
        result = await self.session.execute(select(UserModel).where(UserModel.user_id == user_id))
        return result.scalars().first()
    async def get_news(self) -> List[NewsModel]:
        result = await self.session.execute(select(NewsModel))
        print(type(result))
        return result.scalars().all()

    async def add_category_news(self,category_name:str):
        result = await self.session.execute(select(CategoryModel).where(CategoryModel.category_name == category_name))
        result=result.scalars().first()
        if not result:
            raise HTTPException(status_code=401,detail="Category Not Found")
        return result

    async def add_tag_to_news(self,tag_name:str):
        result = await self.session.execute(select(TagModel).where(TagModel.tag_name == tag_name))
        result=result.scalars().first()
        if not result:
            raise HTTPException(status_code=401,detail="No Such Tag exists")
        return result


    async def add_news_to_category(self, category_name:str, news:NewsModel):
        result=await self.session.execute(select(CategoryModel).where(CategoryModel.category_name==category_name))
        result=result.scalars().first()
        result.news.append(news)
        await self.save_category(result)

    async def get_news_active(self) -> List[NewsModel]:
        result = await self.session.execute(select(NewsModel).where(NewsModel.is_active == True))
        return result.scalars().all()

    async def get_newsid(self, news_id: UUID) -> NewsModel:
        result = await self.session.execute(select(NewsModel).where(NewsModel.news_id == news_id))
        return result.scalars().first()

    async def delete_news(self, news_id: UUID):
        news = await self.get_newsid(news_id=news_id)
        await self.session.delete(news)
        await self.session.commit()
        return {"message": "News Deleted"}

    async def update_news(self, news_id: UUID, request: CreateNewsRequestSchema,news_category:List[CategoryModel],news_tag:List[TagModel]):
        news = await self.get_newsid(news_id=news_id)
        news.news_title=request.news_title
        news.news_content=request.news_content
        news.is_active=True
        news.publish_date=request.publish_date
        news.city_id=request.city_id
        news.category=news_category
        news.tags=news_tag
        self.session.add(news)
        await self.session.commit()
        await self.session.refresh(news)
        return news


