from uuid import UUID

from fastapi import Depends, HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from core import db_session
from app.v1.models import NewsModel,TagModel

class TagsNewsRepo:
    def __init__(self,db:AsyncSession=Depends(db_session)):
        self.session=db

    async def get_news(self, news_id:UUID):
        check = await self.session.execute(select(NewsModel).where(NewsModel.news_id==news_id))
        news = check.scalars().first()
        if not news:
            raise HTTPException(status_code=404, detail="No Such News Exist")
        else:
            return news

    async def get_tag(self, tag_id:UUID):
        check = await self.session.execute(select(TagModel).where(TagModel.tag_id==tag_id))
        tag = check.scalars().first()
        if not tag:
           raise HTTPException(status_code=404, detail="No Such Tag Exist")
        else:
            return tag


    async def add_tag_to_news(self, news:NewsModel):
        self.session.add(news)
        await self.session.commit()
        return news
