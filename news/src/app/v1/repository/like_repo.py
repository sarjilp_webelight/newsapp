from typing import List
from uuid import UUID

from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.v1.models import LikeModel, NewsModel
from app.v1.schema import CreateLikeRequestSchema
from core import db_session


class LikeRepo:
    def __init__(self, db: AsyncSession = Depends(db_session)):
        self.session = db

    async def save(self, like: LikeModel) -> LikeModel:
        self.session.add(like)
        return like

    async def get_like(self) -> List[LikeModel]:
        result = await self.session.execute(select(LikeModel))
        return result.scalars().all()

    async def get_likeid(self, like_id: UUID) -> LikeModel:
        result = await self.session.execute(select(LikeModel).where(LikeModel.like_id == like_id))
        return result.scalars().first()

    async def delete_like(self, like_id: UUID):
        like = await self.get_likeid(like_id=like_id)
        await self.session.delete(like)
        await self.session.commit()
        return {"message": "Like Deleted"}


    async def getLikeCount(self,news_id:UUID):
        news=await self.session.execute(select(NewsModel).where(NewsModel.news_id==news_id))
        return news.scalars().first()

    async def setlikeCount(self,news:NewsModel):
        self.session.add(news)
        await self.session.commit()


    async def check_like_unlike(self,user_id:UUID,news_id):
        check=await self.session.execute(select(LikeModel).where(LikeModel.news_id==news_id and LikeModel.user_id==user_id))
        return check.scalars().first()