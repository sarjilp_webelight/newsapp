from fastapi import Depends
from typing import List
from uuid import UUID
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from app.v1.schema import CreateUserRequestSchema,CreateUserResponse

from app.v1.models import UserModel
from core import db_session

class UserRepo:
    def __init__(self,db:AsyncSession=Depends(db_session)):
        self.session=db


    async def save(self,user:UserModel)->CreateUserResponse:
        user= self.session.add(user)
        await self.session.commit()
        return user

    async def get_all(self) -> List[UserModel]:
        result = await self.session.execute(select(UserModel))
        return result.scalars().all()

    async def get_id(self,user_id:UUID):
        result = await self.session.execute(select(UserModel).where(UserModel.user_id==user_id))
        return result.scalars().first()


    async def delete_byid(self,user_id:UUID):
        user = await self.get_id(user_id=user_id)
        await self.session.delete(user)
        await self.session.commit()
        return {"message":"User Deleted"}



    async def update_user(self,user_id:UUID,request:CreateUserRequestSchema):
        user=await self.get_id(user_id=user_id)
        for var, value in vars(request).items():
            setattr(user,var,value) if value else None

        self.session.add(user)
        await self.session.commit()
        await self.session.refresh(user)
        return user





