from fastapi import Depends
from typing import List
from sqlalchemy.ext.asyncio import AsyncSession
from core import db_session
from sqlalchemy import select
from uuid import UUID
from app.v1.models import RoleModel
from app.v1.schema import CreateRoleRequestSchema

class RoleRepo:

    def __init__(self,db:AsyncSession=Depends(db_session)):
        self.session=db

    async def save(self,role:RoleModel)->RoleModel:
        self.session.add(role)
        return role


    async def get_all_news(self)->List[RoleModel]:
        result = await self.session.execute(select(RoleModel))
        return result.scalars().all()


    async def get_role_id(self,role_id:UUID):
        result = await self.session.execute(select(RoleModel).where(RoleModel.role_id==role_id))
        return result.scalars().first()


    async def delete_role(self,role_id:UUID):
        role = await self.get_role_id(role_id)
        await self.session.delete(role)
        await self.session.commit()
        return {"message":"Role Deleted"}


    async def update_role(self,role_id:UUID,request:CreateRoleRequestSchema):
        role=await self.get_role_id(role_id)
        for var,value in vars(request).items():
            setattr(role,var,value) if value else None

        self.session.add(role)
        await self.session.commit()
        return role