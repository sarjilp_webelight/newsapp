from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID
from app.v1.models import CityModel
from core import db_session
from core.db.session import async_session_factory
from sqlalchemy import select
from app.v1.schema import CreateCityRequestSchema

class CityRepo:
    # def __init__(self):

    async def save(self, city: CityModel) -> CityModel:

        async with async_session_factory() as session:
            session.add(city)
            await session.commit()
            return city


    async def get_city(self):
        async with async_session_factory() as session:
            city=await session.execute(select(CityModel))
            return city.scalars().all()

    async def get_cityid(self,city_id:UUID):
        async with async_session_factory() as session:
            city=await session.execute(select(CityModel).where(CityModel.city_id==city_id))
            return city.scalars().first()


    async def delete_city(self,city_id:UUID):
        async with async_session_factory() as session:
            city= await self.get_cityid(city_id)
            await session.delete(city)
            await session.commit()
            return {"message":"City Deleted"}

    async def update_city(self,city_id:UUID,request:CreateCityRequestSchema):
        async with async_session_factory() as session:
            city = await self.get_cityid(city_id)
            for var, value in vars(request).items():
                setattr(city,var,value) if value else None

            session.add(city)
            await session.commit()
            return city


