from fastapi import Depends,status, Request
from fastapi.security import HTTPBearer
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from app.v1.schema import CreateCommentRequestSchema,CreateCommentResponse
from app.v1.service import CommentCommandService
from uuid import UUID

from core.utils.role import GetRole
from core.utils.token import Token

router=InferringRouter()
security=HTTPBearer()
@cbv(router=router)
class CommentController:
    comment_command_service:CommentCommandService=Depends(CommentCommandService)
    @router.post(
        "/addComment",
        status_code=status.HTTP_200_OK,
        response_model=CreateCommentResponse,
        name="Add Comment"
    )
    async def add_comment(self,request:CreateCommentRequestSchema,details=Depends(security)):
        user_details = await GetRole.getUserDetails(details.credentials)
        user_id = user_details["user_id"]
        news_id=request.news_id
        return await self.comment_command_service.create_comment(comment_content=request.comment_content,user_id=user_id,news_id=news_id,comment_date=request.comment_date)

    @router.get(
        "/getComments",
        status_code=status.HTTP_200_OK,
        name="Get All Comments"
    )
    async def get_all_comments(self):
        return await self.comment_command_service.read_comment()

    @router.get(
        "/getCommentsById/{comment_id}",
        status_code=status.HTTP_200_OK,
        name="Get Comments By Id"
    )
    async def get_commentby_id(self, comment_id: UUID):
        return await self.comment_command_service.read_comment_byid(comment_id)

    @router.delete(
        "/deleteComment/{comment_id}",
        status_code=status.HTTP_200_OK,
        name="Delete A Comment"
    )
    async def delete_comment(self, comment_id: UUID,details=Depends(security)):
        user_details = await GetRole.getUserDetails(details.credentials)
        user_id = user_details["user_id"]

        return await self.comment_command_service.delete_comment(comment_id,user_id=user_id)


    @router.put(
        "/updateComment/{comment_id}",
        status_code=status.HTTP_200_OK,
        response_model=CreateCommentResponse,
        name="Update Comment"
    )
    async def update_comment(self,comment_id:UUID,request:CreateCommentRequestSchema):
        return await self.comment_command_service.update_comment(comment_id,request)