from uuid import UUID

from fastapi import Depends, status, Request, HTTPException
from fastapi.security import HTTPBearer
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter

from app.v1.schema import CreateNewsRequestSchema, CreateNewsResponse
from app.v1.service import NewsCommandService
from core.utils.token import Token
from core.utils.role import Role,GetRole

router = InferringRouter()
security = HTTPBearer()


@cbv(router=router)
class NewsController:
    news_command_service: NewsCommandService = Depends(NewsCommandService)

    @router.post(
        "/addNews",
        status_code=status.HTTP_200_OK,
        response_model=CreateNewsResponse,
        name="Add News"
    )
    async def add_news(self, request: CreateNewsRequestSchema, details=Depends(security)):
        user_details=await GetRole.getUserDetails(details.credentials)
        if user_details["role"]!=Role.Journalist.name:
            raise HTTPException(status_code=401,detail="Only Journalist Can access")
        user_id=user_details["user_id"]
        return await self.news_command_service.create_news(news_title=request.news_title,
                                                           news_content=request.news_content,
                                                           publish_date=request.publish_date, city_id=request.city_id,
                                                           user_id=user_id, categories=request.categories,
                                                           tags=request.tags)

    @router.get(
        "/getNews",
        status_code=status.HTTP_200_OK,
        name="Get All News"
    )
    async def get_all_news(self):
        return await self.news_command_service.read_news()

    @router.get(
        "/getNewsById/{news_id}",
        status_code=status.HTTP_200_OK,
        name="Get News By Id"
    )
    async def get_newsby_id(self, news_id: UUID):
        return await self.news_command_service.read_newsid(news_id)

    @router.delete(
        "/deleteNews/{news_id}",
        status_code=status.HTTP_200_OK,
        name="Delete A News"

    )
    async def delete_news(self, news_id: UUID, details=Depends(security)):
        user_details = await GetRole.getUserDetails(details.credentials)
        user_id = user_details["user_id"]
        return await self.news_command_service.delete_news(news_id, user_id)

    @router.put(
        "/updateNews/{news_id}",
        status_code=status.HTTP_200_OK,
        response_model=CreateNewsResponse,
        name="Update News"
    )
    async def update_news(self, news_id: UUID, request: CreateNewsRequestSchema, details=Depends(security)):
        user_details = await GetRole.getUserDetails(details.credentials)
        user_id = user_details["user_id"]
        return await self.news_command_service.update_news(news_id, request, user_id)

    @router.put(
        "/activateNews/{news_id}",
        status_code=status.HTTP_200_OK,
        name="Activate News"
    )
    async def activate_news(self, news_id: UUID, details=Depends(security)):
        #print(type(details.credentials))
        user_details=await GetRole.getUserDetails(details.credentials)
        if user_details["role"]!=Role.Admin.name and user_details["role"]!=Role.Agency.name :
            raise HTTPException(status_code=401,detail="Only Agency/Admin Can access")
        return await self.news_command_service.activate_news(news_id)

    @router.get(
        "/getNewsbyTags/{tag_name}",
        status_code=status.HTTP_200_OK,
        name="Get News by Tag"
    )
    async def get_news_by_tags(self,tag_name:str):
        return await self.news_command_service.get_news_by_tags(tag_name)

    @router.get(
        "/getNewsbyCategories/{category_name}",
        status_code=status.HTTP_200_OK,
        name="Get News by Category"
    )
    async def get_news_by_category(self, category_name: str):
        return await self.news_command_service.get_news_by_category(category_name)
