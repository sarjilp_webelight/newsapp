from fastapi import Depends,status
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from app.v1.schema import CategoryNewsRequestSchema,CreateNewsResponse
from app.v1.service import CategoryNewsService

router=InferringRouter()

@cbv(router=router)
class CategoryNewsController:
    category_news_service:CategoryNewsService=Depends(CategoryNewsService)
    @router.post(
        "/addNewsCategory",
        status_code=status.HTTP_200_OK,
        response_model=CreateNewsResponse,
        name="Add Category to News"
    )
    async def add_news_category(self,request:CategoryNewsRequestSchema):

        return await self.category_news_service.add_news_category(**request.dict())