from app.v1.controller.user_controller import router as user_router
from app.v1.controller.news_controller import router as news_router
from app.v1.controller.city_controller import router as city_router
from app.v1.controller.role_controller import router as role_router
from app.v1.controller.tag_controller import router as tag_router
from app.v1.controller.like_controller import router as like_router
from app.v1.controller.category_controller import router as category_router
from app.v1.controller.comment_controller import router as comment_router
from app.v1.controller.auth_controller import router as auth_router
from app.v1.controller.category_news_controller import router as category_news_router
from app.v1.controller.tags_news_controller import router as tags_news_router


__all__=[
    "category_news_router",
    "a_router",
    "user_router",
    "news_router",
    "city_router",
    "role_router",
    "tag_router",
    "like_router",
    "category_router",
    "comment_router",
    "auth_router",
    "tags_news_router"
]