from uuid import UUID
from typing import List
from fastapi import Depends, status, HTTPException
from fastapi.security import HTTPBearer
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter

from core.utils.role import Role, GetRole
from app.v1.schema import CreateUserRequestSchema, CreateUserResponse
from app.v1.service import UserCommandService

router = InferringRouter()
security = HTTPBearer()

@cbv(router=router)
class UserController:
    user_command_services: UserCommandService = Depends(UserCommandService, use_cache=True)

    @router.post(
        "/addUser",
        status_code=status.HTTP_200_OK,
        response_model=CreateUserResponse,
        name="Add User"
    )
    async def add_user(self, request: CreateUserRequestSchema,details=Depends(security)):
        user_details=await GetRole.getUserDetails(details.credentials)
        if user_details["role"]!=Role.Admin.name :
            raise HTTPException(status_code=401,detail="Only Admin Can access")
        res =await self.user_command_services.create_user(**request.dict())
        return res

    @router.get(
        "/getUsers",
        status_code=status.HTTP_200_OK,
        response_model=List[CreateUserResponse],
        name="Get All Users"
    )
    async def get_all_users(self):
        return await self.user_command_services.read_users()

    @router.get(
        "/getUserById/{user_id}",
        status_code=status.HTTP_200_OK,
        response_model=CreateUserResponse,
        name="Get User By Id"
    )
    async def get_by_id(self, user_id: UUID):
        return await self.user_command_services.get_by_id(user_id)

    @router.delete(
        "/deleteUser/{user_id}",
        status_code=status.HTTP_200_OK,
        name="Delete A User"
    )
    async def delete_user(self, user_id: UUID,details=Depends(security)):
        user_details=await GetRole.getUserDetails(details.credentials)
        if user_details["role"]!=Role.Admin.name :
            raise HTTPException(status_code=401,detail="Only Admin Can access")
        return await self.user_command_services.delete_user(user_id)

    @router.put(
        "/updateUser/{user_id}",
        status_code=status.HTTP_200_OK,
        response_model=CreateUserResponse,
        name="Update A User"
    )
    async def update_user(self, user_id: UUID, request: CreateUserRequestSchema,details=Depends(security)):
        user_details=await GetRole.getUserDetails(details.credentials)
        if user_details["role"]!=Role.Admin.name :
            raise HTTPException(status_code=401,detail="Only Admin Can access")
        return await self.user_command_services.update_user(user_id, request)

    @router.put(
        "/allotRole",
        status_code=status.HTTP_200_OK,
        name="Allot Role"
    )
    async def allot_role(self,user_id:UUID,role_id:UUID,details=Depends(security)):
        #print(type(details.credentials))
        user_details=await GetRole.getUserDetails(details.credentials)
        if user_details["role"]!=Role.Admin.name :
            raise HTTPException(status_code=401,detail="Only Admin Can access")
        return await self.user_command_services.allot_role(user_id,role_id)

    @router.put(
        "/activateUser",
        status_code=status.HTTP_200_OK,
        name="Activate User"
    )
    async def activate_user(self,user_id:UUID,details=Depends(security)):
        #print(type(details.credentials))
        user_details=await GetRole.getUserDetails(details.credentials)
        if user_details["role"]!=Role.Admin.name and user_details["role"]!=Role.Agency.name :
            raise HTTPException(status_code=401,detail="Only Agency/Admin Can access")
        return await self.user_command_services.activate_user(user_id)





