from fastapi import Depends,status
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from app.v1.schema import CreateCategoryRequestSchema,CreateCategoryResponse
from app.v1.service import CategoryCommandService
from uuid import UUID

router=InferringRouter()


@cbv(router=router)
class CategoryController:
    category_command_service:CategoryCommandService=Depends(CategoryCommandService)

    @router.post(
        "/addCategory",
        status_code=status.HTTP_200_OK,
        response_model=CreateCategoryResponse,
        name="Add Category"
    )
    async def add_category(self,request:CreateCategoryRequestSchema):
        return await self.category_command_service.create_category(**request.dict())

    @router.get(
        "/getCategory",
        status_code=status.HTTP_200_OK,
        name="Get All Categories"
    )
    async def get_category(self):
        return await self.category_command_service.get_category()

    @router.get(
        "/getCategoryById/{category_id}",
        status_code=status.HTTP_200_OK,
        name="Get Category By Id"
    )
    async def get_category_byid(self,category_id:UUID):
        return await self.category_command_service.get_category_byid(category_id)

    @router.delete(
        "/deleteCategory/{category_id}",
        status_code=status.HTTP_200_OK,
        name="Delete A Category"
    )
    async def delete_category(self,category_id:UUID):
        return await self.category_command_service.delete_category(category_id)

    @router.put(
        "/updateCategory/{category_id}",
        status_code=status.HTTP_200_OK,
        name="Update A Category"
    )
    async def update_category(self,category_id:UUID,request:CreateCategoryRequestSchema):
        return await self.category_command_service.update_category(category_id,request)

