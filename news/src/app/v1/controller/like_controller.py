from fastapi import Depends,status,Request
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from app.v1.schema import CreateLikeRequestSchema,CreateLikeResponse
from app.v1.service import LikeCommandService
from uuid import UUID
from fastapi.security import HTTPBearer

from core.utils.role import GetRole
from core.utils.token import Token


router=InferringRouter()
security=HTTPBearer()
@cbv(router=router)
class LikeController:
    like_command_service:LikeCommandService=Depends(LikeCommandService)
    @router.post(
        "/addLikes",
        status_code=status.HTTP_200_OK,
        response_model=CreateLikeResponse,
        name="Add Likes"
    )
    async def add_likes(self,request:CreateLikeRequestSchema):
        return await self.like_command_service.create_like(**request.dict())

    @router.get(
        "/getLikes",
        status_code=status.HTTP_200_OK,
        name="Get All Likes"
    )
    async def get_all_likes(self):
        return await self.like_command_service.get_like()


    @router.delete(
        "/deleteLikes/{like_id}",
        status_code=status.HTTP_200_OK,
        name="Delete A Like"
    )
    async def delete_likes(self, like_id: UUID):
        return await self.like_command_service.delete_like(like_id)


    @router.post(
        "/likeUnlikeNews",
        status_code=status.HTTP_200_OK,
        name="Like-Unlike News",
        dependencies=[Depends(security)]
    )
    async def likeUnlikeNews(self,news_id:UUID,details=Depends(security)):
        user_details = await GetRole.getUserDetails(details.credentials)
        user_id = user_details["user_id"]
        return await self.like_command_service.check_like_unlike(user_id=user_id,news_id=news_id)