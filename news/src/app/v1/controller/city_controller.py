from typing import List

from fastapi_utils.inferring_router import InferringRouter
from fastapi import status
from app.v1.schema import CreateCityRequestSchema,CreateCityResponse
from app.v1.service import CityCommandService
router = InferringRouter()
from uuid import UUID


@router.post(
    "/addCity",
    status_code=status.HTTP_200_OK,
    response_model= CreateCityResponse,
    name="Add City"
)
async def add_city(request:CreateCityRequestSchema, ):
    city_command_service=CityCommandService()
    return await city_command_service.create_city(**request.dict())


@router.get(
    "/getCity",
    status_code=status.HTTP_200_OK,
    response_model=List[CreateCityResponse],
    name="Get Cities"
)
async def get_city():
    city_command_service=CityCommandService()
    return await city_command_service.get_city()


@router.get(
    "/getCityById/{city_id}",
    status_code=status.HTTP_200_OK,
    response_model=CreateCityResponse,
    name="Get City By Id"
)
async def get_city_byid(city_id:UUID):
    city_command_service=CityCommandService()
    return await city_command_service.get_city_byid(city_id)


@router.delete(
    "/deleteCity/{city_id}",
    status_code=status.HTTP_200_OK,
    name="Delete City"
)
async def delete_city(city_id:UUID):
    city_command_service=CityCommandService()
    return await city_command_service.delete_city(city_id)


@router.put(
    "/updateCity/{city_id}",
    status_code=status.HTTP_200_OK,
    name="Update City"
)
async def update_city(city_id:UUID,request:CreateCityRequestSchema):
    city_command_service=CityCommandService()
    return await city_command_service.update_city(city_id,request)




