from fastapi import Depends,status
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from app.v1.schema import CreateTagRequestSchema,CreateTagResponse
from app.v1.service import TagCommandService
from uuid import UUID

router=InferringRouter()


@cbv(router=router)
class TagController:
    tag_command_service:TagCommandService=Depends(TagCommandService)

    @router.post(
        "/addTag",
        status_code=status.HTTP_200_OK,
        response_model=CreateTagResponse,
        name="Add Tag"
    )
    async def add_tag(self,request:CreateTagRequestSchema):
        return await self.tag_command_service.create_tag(**request.dict())

    @router.get(
        "/getTags",
        status_code=status.HTTP_200_OK,
        name="Get All Tags"
    )
    async def get_tags(self):
        return await self.tag_command_service.get_tags()


    @router.delete(
        "/deleteTag/{tag_id}",
        status_code=status.HTTP_200_OK,
        name="Delete A Tag"
    )
    async def delete_tag(self,tag_id:UUID):
        return await self.tag_command_service.delete_tag(tag_id)


