from fastapi import Depends,status
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from app.v1.schema import CreateNewsResponse, TagsNewsRequestSchema
from app.v1.service import TagsNewsService

router=InferringRouter()

@cbv(router=router)
class TagsNewsController:
    tags_news_service:TagsNewsService=Depends(TagsNewsService)
    @router.post(
        "/addNewsTag",
        status_code=status.HTTP_200_OK,
        response_model=CreateNewsResponse,
        name="Add Tags to News"
    )
    async def add_news_tags(self,request:TagsNewsRequestSchema):
        return await self.tags_news_service.add_tag_to_news(**request.dict())