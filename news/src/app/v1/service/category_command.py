from fastapi import Depends
from app.v1.repository import CategoryRepo
from app.v1.models import CategoryModel
import uuid
from uuid import UUID
from app.v1.schema import CreateCategoryRequestSchema,CreateCategoryResponse


class CategoryCommandService:
    def __init__(self,category_repo:CategoryRepo=Depends(CategoryRepo)):
        self.category_repo=category_repo


    async def create_category(
            self,
            category_name:str
    ):
        category=CategoryModel.create(category_name)
        return await self.category_repo.save(category=category)

    async def get_category(self):
        return await self.category_repo.get_category()


    async def get_category_byid(self,category_id:UUID):
        return await self.category_repo.get_categoryid(category_id=category_id)

    async def delete_category(self,category_id:UUID):
        return await self.category_repo.delete_category(category_id)

    async def update_category(self,category_id:UUID,request:CreateCategoryRequestSchema):
        return await self.category_repo.update_category(category_id,request)