from uuid import UUID

from app.v1.repository import TagsNewsRepo
from fastapi import Depends



class TagsNewsService:
    def __init__(self,tags_news_repo:TagsNewsRepo=Depends(TagsNewsRepo)):
        self.tags_news_repo=tags_news_repo

    async def add_tag_to_news(self, news_id:UUID, tag_id:UUID):
        news=await self.tags_news_repo.get_news(news_id=news_id)
        tag=await self.tags_news_repo.get_tag(tag_id=tag_id)
        news.tags.append(tag)
        return await self.tags_news_repo.add_tag_to_news(news)