import uuid
from datetime import date
from typing import List
from uuid import UUID

from fastapi import Depends, HTTPException

from app.v1.models import NewsModel
from app.v1.repository import NewsRepo
from app.v1.schema import CreateNewsRequestSchema


class NewsCommandService:
    def __init__(
            self,
            news_repo: NewsRepo = Depends(NewsRepo)
    ):
        self.news_repo = news_repo

    async def create_news(
            self,
            news_title: str,
            news_content: str,
            user_id: UUID,
            publish_date: date,
            city_id: UUID,
            categories: List[str],
            tags: List[str]
    ):
        news = NewsModel.create(news_title=news_title, news_content=news_content,publish_date=publish_date, city_id=city_id, user_id=user_id)

        # news1=NewsModel()
        # news1.news_id
        user = await self.news_repo.check_active_user(user_id)
        if user.is_active != True :
            raise HTTPException(status_code=401, detail="Journalist Not Activated")

        for i in categories:
            await self.news_repo.add_news_to_category(i, news)

        for i in tags:
            tag = await self.news_repo.add_tag_to_news(i)
            news.tags.append(tag)
        news = await self.news_repo.save(news=news)
        await self.news_repo.session.commit()
        return news

    async def read_news(self):
        result = await self.news_repo.get_news_active()
        return result

    async def read_newsid(self, news_id: UUID):
        result = await self.news_repo.get_newsid(news_id)
        return result

    async def delete_news(self, news_id: UUID, user_id: UUID):
        news = await self.news_repo.get_newsid(news_id)
        print(news_id)
        if str(news.user_id) != user_id:
            raise HTTPException(status_code=401, detail="This User cannot delete this news")
        # user= self.user_repo.get_id(user_id=user_id)
        # print(user)
        return await self.news_repo.delete_news(news_id)

    async def update_news(self, news_id: UUID, request: CreateNewsRequestSchema, user_id: UUID):
        news = await self.news_repo.get_newsid(news_id)
        print(news_id)
        if str(news.user_id) != user_id:
            raise HTTPException(status_code=401, detail="This User cannot update this news")
        news_category = []
        news_tag = []
        category = request.categories
        tags = request.tags
        for i in category:
            c = await self.news_repo.add_category_news(i)
            print(c)
            news_category.append(c)

        for i in tags:
            c = await self.news_repo.add_tag_to_news(i)
            print(c)
            news_tag.append(c)

        return await self.news_repo.update_news(news_id, request, news_category, news_tag)

    async def activate_news(self, news_id: UUID):
        news = await self.news_repo.get_newsid(news_id)
        if news.is_active == True:
            raise HTTPException(status_code=401, detail="New is already active")
        news.is_active = True
        await self.news_repo.save(news)
        return {"message": "News Activated"}

    async def get_news_by_tags(self, tag_name: str):
        tag = await self.news_repo.add_tag_to_news(tag_name)
        return tag.news

    async def get_news_by_category(self, category_name: str):
        category = await self.news_repo.add_category_news(category_name)
        return category.news
