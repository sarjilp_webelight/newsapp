from fastapi import Depends
from app.v1.repository import LikeRepo
from app.v1.models import LikeModel
import uuid
from uuid import UUID
from app.v1.schema import CreateLikeRequestSchema,CreateLikeResponse

class LikeCommandService:
    def __init__(
            self,
            like_repo:LikeRepo=Depends(LikeRepo)
    ):
        self.like_repo=like_repo

    async def create_like(
            self,
            user_id:UUID,
            news_id:UUID
    ):
        like=LikeModel.create(user_id=user_id,news_id=news_id)
        like=await self.like_repo.save(like=like)
        await self.like_repo.session.commit()
        return like

    async def get_like(self):
        result= await self.like_repo.get_like()
        return result


    async def get_likeid(self,like_id:UUID):
        result = await self.like_repo.get_likeid(like_id)
        return result



    async def delete_like(self,like_id:UUID):
        return await self.like_repo.delete_like(like_id)


    async def check_like_unlike(self,user_id:UUID,news_id:UUID):
        check = await self.like_repo.check_like_unlike(user_id=user_id,news_id=news_id)
        if not check:
            await self.create_like(user_id=user_id,news_id=news_id)
            news=await self.like_repo.getLikeCount(news_id)
            news.like_count=news.like_count+1
            await self.like_repo.setlikeCount(news)
            return {"message":"News Liked"}
        else:
            like_id=check.like_id
            await self.delete_like(like_id=like_id)
            news = await self.like_repo.getLikeCount(news_id)
            news.like_count = news.like_count - 1
            await self.like_repo.setlikeCount(news)
            return {"message":"News Unliked"}
