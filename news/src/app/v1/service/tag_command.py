from fastapi import Depends
from app.v1.repository import TagRepo
from app.v1.models import TagModel
import uuid
from uuid import UUID
from app.v1.schema import CreateTagRequestSchema,CreateTagResponse


class TagCommandService:
    def __init__(self,tag_repo:TagRepo=Depends(TagRepo)):
        self.tag_repo=tag_repo


    async def create_tag(
            self,
            tag_name:str
    ):
        tag=TagModel.create(tag_name)
        tag= await self.tag_repo.save(tag=tag)
        await self.tag_repo.session.commit()
        return tag

    async def get_tags(self):
        return await self.tag_repo.get_tags()


    async def get_tagid(self,tag_id:UUID):
        return await self.tag_repo.get_tagid(tag_id=tag_id)

    async def delete_tag(self,tag_id:UUID):
        return await self.tag_repo.delete_tag(tag_id)

