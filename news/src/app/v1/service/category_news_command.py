from uuid import UUID

from app.v1.repository import CategoryNewsRepo
from fastapi import Depends



class CategoryNewsService:
    def __init__(self,category_news_repo:CategoryNewsRepo=Depends(CategoryNewsRepo)):
        self.category_news_repo=category_news_repo

    async def add_news_category(self, news_id:UUID, category_id:UUID):
        news=await self.category_news_repo.get_news(news_id=news_id)
        category=await self.category_news_repo.get_category(category_id=category_id)
        news.category.append(category)
        return await self.category_news_repo.add_category_to_news(news)