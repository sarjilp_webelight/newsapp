import uuid
from uuid import UUID

from fastapi import Depends, HTTPException

from app.v1.models import UserModel
from app.v1.repository import UserRepo
from app.v1.schema import CreateUserRequestSchema


class UserCommandService:
    def __init__(
            self,
            user_repo: UserRepo = Depends(UserRepo)
    ):
        self.user_repo = user_repo

    async def create_user(
            self,
            email: str,
            password:str,
            fname: str,
            lname: str,

            city_id: UUID
    ):
        user = UserModel.create(email=email,password=password,fname=fname,lname=lname,city_id=city_id)
        print(user)
        await self.user_repo.save(user=user)
        await self.user_repo.session.commit()
        return user

    async def read_users(self):
        return await self.user_repo.get_all()

    async def get_by_id(self, user_id: UUID):
        return await self.user_repo.get_id(user_id)

    async def delete_user(self, user_id: UUID):
        return await self.user_repo.delete_byid(user_id)

    async def update_user(self, user_id: UUID, request: CreateUserRequestSchema):
        return await self.user_repo.update_user(user_id, request)


    async def allot_role(self,user_id:UUID,role_id:UUID):
        user=await self.user_repo.get_id(user_id)
        if not user:
            raise HTTPException(status_code=401,detail="No Such User exists")
        else:
            user.role_id=role_id
            print(user.role_id)
            await self.user_repo.save(user)

        return {"message":"New Role Assigned"}

    async def activate_user(self,user_id:UUID):
        user=await self.user_repo.get_id(user_id)
        user.is_active=True
        await self.user_repo.save(user)
        return {"message":"Journalist Activated"}
