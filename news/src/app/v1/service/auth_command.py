import random
import re
from datetime import datetime, timedelta
from uuid import UUID

from fastapi import Depends, HTTPException, Request

from app.v1.models import UserModel, OtpModel
from app.v1.repository import AuthRepo
from core.utils.hashing import Hasher
from core.utils.token import Token


class AuthCommandService:
    def __init__(self, auth_repo: AuthRepo = Depends(AuthRepo)):
        self.auth_repo = auth_repo

    async def signupuser(self, email: str, password: str, fname: str, lname: str, city_id: UUID):

        check = await self.auth_repo.check_email(email)
        print(check)
        if (not check):
            pass
        else:
            raise HTTPException(status_code=404, detail="User Already Exists")
        user = UserModel.create(email=email, password=password, fname=fname, lname=lname, city_id=city_id)
        await self.auth_repo.save(user)
        return user

    async def login(self, email: str, password: str):
        check = await self.auth_repo.check_email(email)
        if not check:
            raise HTTPException(status_code=404, detail="No Such User Exists")
        isUser = Hasher.verify_password(password, check.password)
        if isUser:
            print("hey")
        else:
            raise HTTPException(status_code=404, detail="Invalid Password")
        del check.news
        del check.comments
        del check.likes
        user_dict = check.__dict__
        del user_dict['_sa_instance_state']
        user_dict["user_id"] = str(user_dict["user_id"])
        user_dict["role_id"] = str(user_dict["role_id"])
        user_dict["city_id"] = str(user_dict["city_id"])
        # print(type(user_dict["user_id"]))
        token = Token.get_token(user_dict)
        return token

    async def check_password(self, value: str):
        if len(value) < 8:
            raise HTTPException(status_code=404, detail="Invalid Password Format")
        if " " in value:
            raise HTTPException(status_code=404, detail="Invalid Password Format")
        if not re.search(r'[A-Z]', value):
            raise HTTPException(status_code=404, detail="Invalid Password Format")
        if not re.search(r'[0-9]', value):
            raise HTTPException(status_code=404, detail="Invalid Password Format")
        if "@" in value or "#" in value or "!" in value or "$" in value or "%" in value or "^" in value or "&" in value or "*" in value:
            return True
        else:
            raise HTTPException(status_code=404, detail="Invalid Password Format")

    async def reset(self, old_password: str, new_password: str, request: Request):
        token = request.headers["authorization"]
        user_details = Token.get_dict(token)
        print(user_details)
        user = await self.auth_repo.reset(user_details)
        print(user.password)
        isUser = Hasher.verify_password(old_password, user.password)
        if not isUser:
            raise HTTPException(status_code=404, detail="Wrong Old Password Added")
        checkPassword = await self.check_password(new_password)
        if (checkPassword):
            new_password = Hasher.get_password_hash(new_password)
            user.password = new_password
            return await self.auth_repo.update_password(user)

    async def get_otp(self, email: str):
        # print("check command")
        check = await self.auth_repo.check_email_for_otp(email)
        if check:
            otp = random.randint(1000, 9999)
            check.otp = otp
            time_limit = timedelta(minutes=15)
            exp_date = datetime.utcnow() + time_limit
            check.exp_date = exp_date
            return await self.auth_repo.add_update_otp(check)
        else:
            otp = OtpModel.create(email)
            return await self.auth_repo.add_update_otp(otp)

    async def forget(self, otp: int, email: str, new_password: str):
        otp_details = await self.auth_repo.get_from_otp(email)
        current_time = datetime.utcnow()
        exp_date = otp_details.exp_date
        if otp == otp_details.otp:
            if current_time > exp_date:
                raise HTTPException(status_code=401, detail="Otp Expired")
            else:
                user = await self.auth_repo.reset(email)
                checkPassword = await self.check_password(new_password)
                if checkPassword:
                    hashed_pswd = Hasher.get_password_hash(new_password)
                    user.password = hashed_pswd
                    await self.auth_repo.update_password(user)
                    return await self.auth_repo.delete_otp(otp_details)
        else:
            raise HTTPException(status_code=401, detail="Enter Correct Otp")

    async def forget_noemail(self, otp: int, new_password: str):
        otp_details = await self.auth_repo.get_using_otp(otp)
        email = otp_details.email
        current_time = datetime.utcnow()
        exp_date = otp_details.exp_date
        if otp == otp_details.otp:
            if current_time > exp_date:
                raise HTTPException(status_code=401, detail="Otp Expired")
            else:
                user = await self.auth_repo.reset(email)
                checkPassword = await self.check_password(new_password)
                if checkPassword:
                    hashed_pswd = Hasher.get_password_hash(new_password)
                    user.password = hashed_pswd
                    await self.auth_repo.update_password(user)
                    return await self.auth_repo.delete_otp(otp_details)
        else:
            raise HTTPException(status_code=401, detail="Enter Correct Otp")
