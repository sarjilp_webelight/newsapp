import uuid

from app.v1.models import CityModel
from app.v1.repository import CityRepo
from uuid import UUID
from app.v1.schema import CreateCityRequestSchema

class CityCommandService:
    def __init__(self):
        self.city_repo = CityRepo()

    async def create_city(
            self,
            city_name: str
    ):
        city = CityModel.create(city_name)
        city = await self.city_repo.save(city=city)
        # await self.city_repo.session.commit()
        return city


    async def get_city(self):
        return await self.city_repo.get_city()


    async def get_city_byid(self, city_id:UUID):
        return await self.city_repo.get_cityid(city_id)

    async def delete_city(self,city_id:UUID):
        return await self.city_repo.delete_city(city_id)

    async def update_city(self,city_id:UUID,request:CreateCityRequestSchema):
        return await self.city_repo.update_city(city_id,request)