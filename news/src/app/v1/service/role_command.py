from fastapi import Depends
from app.v1.repository import RoleRepo
from app.v1.models import RoleModel
import uuid
from uuid import UUID
from app.v1.schema import CreateRoleRequestSchema
class RoleCommandService:

    def __init__(self,role_repo:RoleRepo=Depends(RoleRepo)):
        self.role_repo=role_repo


    async def create_role(self,role_name:str):
        role=RoleModel.create(role_name)
        await self.role_repo.save(role)
        await self.role_repo.session.commit()
        return role


    async def get_all_roles(self):
        return await self.role_repo.get_all_news()

    async def get_role_id(self,role_id:UUID):
        return await self.role_repo.get_role_id(role_id)


    async def delete_role(self,role_id:UUID):
        return await self.role_repo.delete_role(role_id)


    async def update_role(self,role_id:UUID,request:CreateRoleRequestSchema):
        return await self.role_repo.update_role(role_id,request)