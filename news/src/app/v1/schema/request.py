from typing import List

from pydantic import BaseModel, validator
from uuid import UUID
from datetime import date
import re
class CreateUserRequestSchema(BaseModel):
    """
    Request Schema for creating users.
    """

    email:str
    password:str
    fname:str
    lname:str
    email:str

    city_id:UUID

    @validator("email")
    def check_email(value):
        if "@" not in value or " " in value or "." not in value:
            raise ValueError("Invalid Email Address")
        return value

    @validator("password")
    def check_password(value):
        if len(value) <8:
            raise ValueError("Password should be greater than 7 characters")
        if " " in value:
            raise ValueError("Empty Space Not Allowed")
        if not re.search(r'[A-Z]',value):
            raise ValueError("Atleast 1 Capital Alphabet expected in Password")
        if not re.search(r'[0-9]',value):
            raise ValueError("Atleast One Number Expected in Password")
        if "@" in value or "#" in value or "!" in value or "$" in value or "%" in value or "^" in value or "&" in value or "*" in value:
            return value
        else:
            raise ValueError("Invalid Password Format(No Special Character Found")
        return value

    @validator("fname")
    def check_fname(value):
        if " " in value:
            raise ValueError("First Name should not contain a space")
        return value

    @validator("lname")
    def check_lname(value):
        if " " in value:
            raise ValueError("Last Name should not contain a space")
        return value


    class CreateUserRequestSchema(BaseModel):
        """
        Request Schema for creating users.
        """

        email: str
        password: str
        fname: str
        lname: str
        email: str
        role_id: UUID
        city_id: UUID

        @validator("email")
        def check_email(value):
            if "@" not in value or " " in value or "." not in value:
                raise ValueError("Invalid Email Address")
            return value

        @validator("password")
        def check_password(value):
            if len(value) < 8:
                raise ValueError("Password should be greater than 7 characters")
            if " " in value:
                raise ValueError("Empty Space Not Allowed")
            if not re.search(r'[A-Z]', value):
                raise ValueError("Atleast 1 Capital Alphabet expected in Password")
            if not re.search(r'[0-9]', value):
                raise ValueError("Atleast One Number Expected in Password")
            if "@" in value or "#" in value or "!" in value or "$" in value or "%" in value or "^" in value or "&" in value or "*" in value:
                return value
            else:
                raise ValueError("Invalid Password Format(No Special Character Found")
            return value

        @validator("fname")
        def check_fname(value):
            if " " in value:
                raise ValueError("First Name should not contain a space")
            return value

        @validator("lname")
        def check_lname(value):
            if " " in value:
                raise ValueError("Last Name should not contain a space")
            return value


class LoginSchema(BaseModel):
    email : str
    password : str



class CreateNewsRequestSchema(BaseModel):
    news_title:str
    news_content:str

    publish_date:date
    city_id:UUID
    categories:List[str]
    tags:List[str]




class CreateCityRequestSchema(BaseModel):
    city_name:str



class CreateRoleRequestSchema(BaseModel):
    role_name:str


class CreateTagRequestSchema(BaseModel):
    tag_name:str


class CreateLikeRequestSchema(BaseModel):
    user_id:UUID
    news_id:UUID

class CreateCategoryRequestSchema(BaseModel):
    category_name:str

class CreateCommentRequestSchema(BaseModel):
    comment_content:str
    news_id:UUID
    comment_date:date


class CategoryNewsRequestSchema(BaseModel):
    news_id:UUID
    category_id:UUID

class TagsNewsRequestSchema(BaseModel):
    news_id:UUID
    tag_id:UUID






