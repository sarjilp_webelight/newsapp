from fastapi import APIRouter

from app.v1.controller import tags_news_router,auth_router,comment_router,user_router,like_router,news_router,city_router,category_router,role_router,tag_router, category_news_router
#auth_router.include_router(user_router)
#auth_router.include_router(news_router)

v1_router = APIRouter()

v1_router.include_router(auth_router,tags=["Authentication"])
v1_router.include_router(auth_router,tags=["Authentication"])
v1_router.include_router(user_router,prefix="/user",tags=["Users"])
v1_router.include_router(news_router,prefix="/news",tags=["News"])
v1_router.include_router(city_router,prefix="/city",tags=["City"])
v1_router.include_router(role_router,prefix="/role",tags=["Role"])
v1_router.include_router(tag_router,prefix="/tag",tags=["Tag"])
v1_router.include_router(like_router,prefix="/like",tags=["Like"])
v1_router.include_router(category_router,prefix="/category",tags=["Categories"])
v1_router.include_router(comment_router,prefix="/comment",tags=["Comment"])
v1_router.include_router(category_news_router,prefix="/categoryNews",tags=["Category to News"])
v1_router.include_router(tags_news_router,prefix="/tagNews",tags=["Tags to News"])


__all__ = ["v1_router"]
