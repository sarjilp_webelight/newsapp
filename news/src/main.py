import uvicorn

from config import AppEnvironment, config


def main():
    """uvicorn run command"""
    uvicorn.run(
        app="app.server:app",
        host=config.POST_HOST,
        port=config.POST_PORT,
        reload=False if config.ENV == AppEnvironment.Production else True,
        workers=1,
    )


if __name__ == "__main__":
    main()
