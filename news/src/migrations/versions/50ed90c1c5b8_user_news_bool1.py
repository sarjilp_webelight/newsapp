"""User News Bool1

Revision ID: 50ed90c1c5b8
Revises: 52c746b83076
Create Date: 2023-02-06 13:10:24.023067

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '50ed90c1c5b8'
down_revision = '52c746b83076'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('News', 'is_active')
    op.drop_column('Users', 'is_active')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('Users', sa.Column('is_active', sa.VARCHAR(length=70), autoincrement=False, nullable=True))
    op.add_column('News', sa.Column('is_active', sa.VARCHAR(length=70), autoincrement=False, nullable=True))
    # ### end Alembic commands ###
