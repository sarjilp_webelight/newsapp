import os
import secrets
from enum import Enum
from typing import Any, Dict, List, Optional, Union

from pydantic import AnyHttpUrl, BaseSettings, EmailStr, PostgresDsn, validator


class AppEnvironment(str, Enum):
    """
    An Enum class defining the development environments.
    """

    Local = "local"
    Development = "Development"
    Production = "Production"
    Test = "Test"


class Settings(BaseSettings):
    """
    A settings class for the project defining all the necessary parameters within the
    app through a object.
    """

    API_V1_STR: str = "/api/v1"
    # SECRET_KEY: str = secrets.token_urlsafe(32)
    # ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8
    POST_HOST: str = os.getenv("POST_HOST")
    POST_PORT: int = os.getenv("POST_PORT")
    ENV: AppEnvironment = os.environ.get("ENV", "local")
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        """
        Validate CORS origins from the .env files is it exists.
        """
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    POSTGRES_SERVER: str = os.getenv("POSTGRES_SERVER")
    POSTGRES_PORT: str = os.getenv("POSTGRES_PORT")
    POSTGRES_USER: str = os.getenv("POSTGRES_USER")
    POSTGRES_PASSWORD: str = os.getenv("POSTGRES_PASSWORD")
    POSTGRES_DB: str = os.getenv("POSTGRES_DB")
    # POSTGRES_USER_DB: str = os.getenv("POSTGRES_USER_DB")
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None
    SECRET_KEY:str= os.getenv("SECRET_KEY")
    ACCESS_TOKEN_EXPIRE_MINUTES:int = os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES")
    ALGORITHM:str = os.getenv("ALGORITHM")

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        """
        Create a Database URL from the settings provided in the .env file.
        """

        db = values.get("POSTGRES_DB")
        print(db)
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql+asyncpg",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            port=values.get("POSTGRES_PORT"),
            path="/{db}".format(db=db),
        )


    class Config:
        """
        Configuration for the pydantic :class:`BaseSettings`
        """

        case_sensitive = True


config = Settings(_env_file=os.path.realpath("../../.env"))
